package db2;

import java.sql.*;

public class DBDriver {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/java";
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_USER = "java";
    private static final String DB_PASS = "java@Fire";

    private static DBDriver inst;
    private Connection con;

    public Connection getConnection() {
        return con;
    }

    private DBDriver() throws SQLException {
        try {
            Class.forName(JDBC_DRIVER);
            con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);

        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            System.err.println("Insane environment");
            System.exit(0);
        }
    }

    public static synchronized DBDriver getDriver() throws SQLException {
        if (inst == null) {
            inst = new DBDriver();
        }

        return inst;
    }

    public synchronized ResultSet query(String query) throws SQLException {
        PreparedStatement p = con.prepareStatement(query);
        ResultSet ret = null;

        //at this point, we must close p
        //so if a query doesn't produce ResultSet, close it, and rethrow
        //if it does, the client will call closeResult
        try {
            ret = p.executeQuery(query);
        } catch (SQLException e) {
            try {
                p.close();
            } catch (SQLException e2) {
            }

            throw e;
        }

        return ret;
    }

    public synchronized int update(String query) throws SQLException {
        Statement statement = con.createStatement();

        //we want to close the statement here
        int result = -1;
        try {
            result = statement.executeUpdate(query);
        } finally {
            try {
                statement.close();
            } catch (SQLException e) {
            }
        }

        return result;

    }

    public void closeResult(ResultSet r) {
        //maybe I didn't get this correctly...
        //but closing ResultSet alone is not enough
        //and we have to close the statement as well

        try {
            r.getStatement().close();
        } catch (SQLException e) {
        }

        try {
            r.close();
        } catch (SQLException e) {
        }
    }
}
