package dekorator2;

import java.awt.Point;

public abstract class Dekorator extends UżytkownikDrogi {
    protected UżytkownikDrogi user;
    
    public Dekorator(UżytkownikDrogi user) {
        this.user = user;
    }

    @Override
    public Point getLocation() {
        return user.getLocation();
    }

    @Override
    public Point getNextLocation() {
        return user.getNextLocation();
    }

    @Override
    public char getToken() {
        return user.getToken();
    }

    
}
