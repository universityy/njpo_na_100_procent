
package fabryka1;

import java.io.OutputStreamWriter;


public class Main {

    public static void main(String[] args) {
        
        
        try {
            WebPage wp;
            
            wp = PageFactory.createPage(PageType.INFO);
            wp.writeToFile("info.html");
            
            wp = PageFactory.createPage(PageType.NEWS);
            wp.writeToFile("news.html");
            
            wp = PageFactory.createPage(PageType.CONTACT);
            wp.writeToFile("contact.html");
            
            wp = PageFactory.createPage(PageType.GALLERY);
            wp.writeToFile("gallery.html");
            
        } catch (Exception e) {
            
        }
         
        
    }

}
