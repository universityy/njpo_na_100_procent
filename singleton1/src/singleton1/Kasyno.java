package singleton1;

public class Kasyno {
    private double stanKasy = 0;
    private static Kasyno inst;
    
    private Kasyno(double kasa) {
        stanKasy = kasa;
    }
    
    public static synchronized Kasyno inst() {
        if (inst == null) {
            inst = new Kasyno(1000000);
        }
        return inst;
    } 
    
    public double getStanKasy() {
        return stanKasy;
    }
    
    public void przegrana(double stawka) {
        stanKasy += stawka;
    }
    
    public double wygrana(double stawka) {
        if (stawka > stanKasy) { //"stawka większa niż.... stan kasy"
            stawka = stanKasy;
        }
        stanKasy -= stawka;

        return stawka;
    }
    
    public void printStanKasy() {
        System.out.println("W kasynie: "+stanKasy+" dolarów Zimbabweńskich.");
    }
    
}
