package watki2;

import java.math.BigInteger;

public class FactRecur implements Runnable {
    private IterProgress report;

    public FactRecur(IterProgress p) {
        report = p;
    }
    
    private BigInteger doFact(BigInteger n) {
        if (n.compareTo(BigInteger.ONE) > 0 && !Thread.currentThread().isInterrupted())
            return n.multiply(doFact(n.subtract(BigInteger.ONE)));
        else return BigInteger.valueOf(-1);
    }

            
    @Override
    public void run() {
        long start = System.currentTimeMillis();
        if (doFact(BigInteger.valueOf(7500)) != BigInteger.valueOf(-1)) {
            report.reportProgress(1, System.currentTimeMillis()-start);
        }
    }
    
    
}
