package watki2;

import java.math.BigInteger;

public class FactIter implements Runnable {

    private IterProgress report;

    public FactIter(IterProgress p) {
        report = p;
    }

    private static BigInteger fact(long n) {
        BigInteger ret = new BigInteger("1");

        for (long t = 1; t <= n && !Thread.currentThread().isInterrupted(); t++) {
            ret = ret.multiply(BigInteger.valueOf(t));
        }

        return Thread.currentThread().isInterrupted() ? BigInteger.valueOf(-1) : ret;
    }

    @Override
    public void run() {
        long start = System.currentTimeMillis();
        if (fact(7500) != BigInteger.valueOf(-1)) {
            report.reportProgress(0, System.currentTimeMillis()-start);
        };
    }

}
