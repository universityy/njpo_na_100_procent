package kolekcje1;

import java.io.Writer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

class FreqEntry implements Comparable {

    public String key;
    public int freq;

    public FreqEntry(String k, int f) {
        key = k;
        freq = f;
    }

    public int compareTo(Object o) {
        FreqEntry f = (FreqEntry) o;
        return f.freq > freq ? 1 : f.freq == freq ? 0 : -1;
    }
}

public class FreqAnalyzer extends Writer {

    private final HashMap<String, Integer> map;

    public FreqAnalyzer() {
        this.map = new HashMap();
    }

    public Vector getVector() {
        Vector<FreqEntry> v = new Vector();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            v.add(new FreqEntry(entry.getKey(), entry.getValue()));
        }
        Collections.sort(v);

        return v;
    }

    public void write(char[] cbuf, int off, int len) {
        for (int t = off; t < off + len; t++) {
            if (map.containsKey(Character.toString(cbuf[t]))) {
                int tmp = map.get(Character.toString(cbuf[t]));
                map.put(Character.toString(cbuf[t]), tmp + 1);
            } else {
                map.put(Character.toString(cbuf[t]), 1);
            }

        }
    }

    @Override
    public void close() {
    }

    @Override
    public void flush() {
    }

}
