package dekorator11;

import java.io.FileReader;


public class Main {

    public static void main(String[] args) {
        try (FileReader fr = new FileReader("/etc/passwd")) {
            int tmp, ret = 0;
            
            while((tmp = fr.read()) != -1) {
                if (tmp == '\n')
                    ret++;
            }
            
            System.out.println("Liczba wierszy: "+ret);
            
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }

    }

}
