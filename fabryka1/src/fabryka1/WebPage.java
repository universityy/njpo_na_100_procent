package fabryka1;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;


public abstract class WebPage {
    
    public abstract void writeToStream(Writer w) throws IOException;
    
    protected String header(String title) {
        return "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"><title>"+title+"</title></head><body>";
    }
    
    protected String endPage() {
        return "</body></html>";
    }
    
    public void writeToFile(String filename) throws IOException {
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename)));
        writeToStream(bw);
        bw.flush();
        bw.close();
    }
    
    
}
