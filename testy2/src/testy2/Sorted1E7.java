package testy2;

import java.util.Arrays;
import java.util.Random;

public class Sorted1E7 {
    private final int ONE_E_SEVEN = (int)1E7;
    private final double[] numbers = new double[ONE_E_SEVEN];
    
    public Sorted1E7() {
        generateNumbers(ONE_E_SEVEN);
        sortNumbers();
    }
    
    private void generateNumbers(int howMany) {
        Random source = new Random();
        
        while(howMany --> 0) {
            numbers[howMany] = source.nextDouble();
        }        
    }
    
    private void sortNumbers() {
        Arrays.sort(numbers);
    }
    
}
