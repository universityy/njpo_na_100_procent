package kolekcje2;

import kolekcje2.Person;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

public class PersonTest {
    
    public PersonTest() {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullConstructor() {
        System.out.println("null constructor");
        Person instance = new Person(null, "Kowalski", "556");

    }
    
    @Test
    public void testGetDisplay() {
        System.out.println("getDisplay");
        assertEquals("Włodek Kowalski", new Person("Włodek", "Kowalski", "556").getDisplay());
        assertEquals("Zbychu", new Person("Zbychu", null, "556").getDisplay());
    }

    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        
        Person p1, p2, p3;
        
        p1 = new Person("Włodek", "Kowalski", "556");
        p2 = new Person("Włodek", "Prudidudi", "556");
        
        assertTrue(p1.compareTo(p2) < 0);
        assertTrue(p2.compareTo(p1) > 0);
        assertEquals(0, p1.compareTo(p1));

        p1 = new Person("Włodek", null, "556");
        p2 = new Person("Piotrek", "Nul", "556");
        p3 = new Person("Gerwazy", "Nul", "556");
        
        assertTrue(p1.compareTo(p2) > 0);
        assertTrue(p2.compareTo(p1) < 0);
        assertEquals(0, p1.compareTo(p1));
        assertTrue(p2.compareTo(p3) > 0);
        assertTrue(p3.compareTo(p2) < 0);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        Person p1 = new Person("Zbigniew", "Wodecki", "707");
        Person p2 = new Person("Zygmunt", "Trzecia Faza", "3");
        Person p3 = new Person("Zygmunt", null, "3");
        Person p4 = new Person("Zygmunt", "Trzecia Faza", "3");
        Person p5 = new Person("Zygmunt", null, "3");
        Person pnull = null;
        
        assertFalse(p1.equals(p2));
        assertFalse(p1.equals(p3));
        assertFalse(p2.equals(p3));
        assertFalse(p2.equals(pnull));
        assertTrue(p2.equals(p4));
        assertTrue(p3.equals(p5));
        assertFalse(p3.equals(new Date()));
    }
    
    @Test
    public void testHash() {
        System.out.println("hashCode");
        
        Person p1 = new Person("Zbigniew", "Wodecki", "707");
        Person p2 = new Person("Zbigniew", "Wodecki", "708");
        Person p3 = new Person("Zbigniew", "Wodeckj", "707");
        Person p4 = new Person("Zbigniey", "Wodecki", "707");
        Person p5 = new Person("Zbigniey", null, "707");
        
        assertFalse(p1.hashCode() == p2.hashCode());
        assertFalse(p1.hashCode() == p3.hashCode());
        assertFalse(p1.hashCode() == p4.hashCode());
        assertFalse(p1.hashCode() == p5.hashCode());
    }
}
