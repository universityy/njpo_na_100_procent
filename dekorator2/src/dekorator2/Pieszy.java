package dekorator2;

import java.awt.Point;

public class Pieszy extends UżytkownikDrogi {
    
    public Pieszy(Point poz) {
        location.x = poz.x;
        location.y = poz.y;
    }

    @Override
    public Point getNextLocation() {
        Point ret = new Point(location);
        ret.x++;
        return ret;
    }

    @Override
    public char getToken() {
        return 'P';
    }
    

}
