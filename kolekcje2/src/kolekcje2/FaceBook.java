package kolekcje2;

import java.util.*;

public class FaceBook {

    private final List<Person> contacts;
    private String id;

    public FaceBook(String id) {
        this.id = id;
        contacts = new ArrayList();
    }

    public FaceBook(String id, ArrayList contacts) {
        this.id = id;
        this.contacts = contacts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Person> getContacts() {
        return contacts;
    }

    public boolean contains(Person p) {
        return contacts.contains(p);
    }

    public void add(Person p) {
        if (!contains(p)) {
            contacts.add(p);
        }
    }
    
    public boolean remove(Person p) {
        return contacts.remove(p);
    }

    public ArrayList<Person> find(String name, String surname, String phone) {
        ArrayList<Person> results = new ArrayList();

        //imperative version :)
        for (Person p : contacts) {
            if ((name == null || p.getName().equals(name))
                    && (surname == null || p.getSurname().equals(surname))
                    && (phone == null || p.getPhone().equals(phone))) {
                results.add(p);
            }
        }
        return results;
    }

    public void sort() {
        Collections.sort(contacts);
    }

    public void clear() {
        contacts.clear();
    }
    
    public void debugContacts() {
        System.out.println("--> Book "+id);
        for(Person p: contacts) {
            System.out.println("-> "+p.getName()+" "+p.getSurname()+" ("+p.getPhone()+")");
        }
        System.out.println("");
    }
  
    
}
