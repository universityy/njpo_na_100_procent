package gui4;

public class PeselValidator {

    public static boolean peselValid(String pesel) {
        if (pesel == null || pesel.length() != 11) {
            return false;
        }
        
        int[] w = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
        int ctrl = 0;

        for (int i = 0; i < 10; i++) {
            char tmp = pesel.charAt(i);
            if (tmp < '0' || tmp > '9') {
                return false;
            }
            ctrl += (tmp - '0') * w[i];
        }

        char pctrl = pesel.charAt(10);
        if (pctrl < '0' || pctrl > '9') {
            return false;
        }
        
        ctrl %= 10;
        ctrl = 10 - ctrl;
        ctrl %= 10;

        return ctrl == (pctrl - '0');
    }
}
