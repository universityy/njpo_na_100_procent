package watki2;

public interface IterProgress {
    public void reportProgress(int type, long computedTime);
}
