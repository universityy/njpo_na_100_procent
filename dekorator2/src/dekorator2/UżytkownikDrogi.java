package dekorator2;

import java.awt.Point;

public abstract class UżytkownikDrogi {
    protected Point location = new Point();

    public abstract char getToken();
    public abstract Point getNextLocation();
    
    public Point getLocation() {
        return location;
    }
    
    public void setLocation(Point location) {
        this.location = location;
    }
}
