package fabryka1;

import java.io.IOException;
import java.io.Writer;

public class ContactPage extends WebPage {

    private static final String[] names = new String[]{"Florian", "Hipolit",
        "Gerwazy", "Protazy", "Sobiesław", "Eustachy"};

    private static final String[] surnames = new String[]{"Popielny", "Kościelny",
        "Przekościelny", "Śródkościelny", "Przykościelny", "Zakościelny", "Kopeć"};

    private static final String[] streets = new String[]{"Zygmunta III Wazy",
        "Zbigniewa Wodeckiego", "Jakuba Świnki", "Św. Wojciecha", "Michaela Jacksona"};

    //thx to wp.pl 4 inspiration :D
    private static final String[] cities = new String[]{"Koziegłowy",
        "Psie Głowy", "Babki Oleckie", "Nowe Rumunki",
        "Swornegacie", "Młynek Nieświński", "Białykał"};

    @Override
    public void writeToStream(Writer w) throws IOException {
        w.write(header("Contact"));

        w.write("My contact info <in Polish>:<br><br>");
        w.write(names[(int) (Math.random() * names.length)] + " "
                + surnames[(int) (Math.random() * surnames.length)] + "<br>");
        w.write("ul. " + streets[(int) (Math.random() * streets.length)] + " "
                + ((int) (Math.random() * 44)) + "<br>");
        w.write("69-666 " + cities[(int) (Math.random() * cities.length)]);
        w.write(endPage());

    }

}
