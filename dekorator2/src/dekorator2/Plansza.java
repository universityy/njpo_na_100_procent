package dekorator2;

import java.awt.Point;
import java.util.ArrayList;

public class Plansza {
    static final int SIZE = 12;
    char[][] plansza = new char[SIZE][SIZE];
    ArrayList<UżytkownikDrogi> userTablica = new ArrayList<>();
    
    private void zeroThePlansza() {
        for(char[] row: plansza)
            for (int t = 0; t < row.length; t++)
                row[t] = 0;
    }
    
    public void addUżytkownik(UżytkownikDrogi user) {
        userTablica.add(user);
    }
    
    public boolean nextTick() {
        zeroThePlansza();
        boolean safe = true;
        
        for(UżytkownikDrogi user: userTablica) {
            Point newLocation = user.getNextLocation();
            newLocation.x = (SIZE + newLocation.x) % SIZE;
            newLocation.y = (SIZE + newLocation.y) % SIZE;
            user.setLocation(newLocation);
            
            if (plansza[newLocation.x][newLocation.y] != 0) {
                safe = false;
                plansza[newLocation.x][newLocation.y] = 'X';
            } else
                plansza[newLocation.x][newLocation.y] = user.getToken();
        }
        
        return safe;
    }
    
    public void printThePlansza() {
        for (int y = 0; y <= SIZE+1; y++) {
            for (int x = 0; x <= SIZE+1; x++) {
                if (x == 0 || x == SIZE+1 || y == 0 || y == SIZE+1)
                    System.out.print('#');
                else
                    System.out.print(plansza[x-1][y-1] == 0 ? ' ' : plansza[x-1][y-1]);
            }
            System.out.println("");
        }
    }
    
    
}
