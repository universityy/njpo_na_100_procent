
package kolekcje3;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;


public class Main {

    public static void main(String[] args) {
        long startA, stopA, startB, stopB;
        int tmp;
        ArrayList<Date> al;
        LinkedList<Date> ll;
        
        System.out.println("ArrayList vs LinkedList benczmark [im mniej tym lepiej]");
        //dzięki temu benczmarkowi, dowiedziałem się, że LinkedList ma coś takiego
        //jak header.previous.element;... sprytne :)
        
        //runda 1
        al = new ArrayList();
        ll = new LinkedList();
        
        for(tmp = 0; tmp<1000; tmp++) {
            al.add(new Date());
            ll.add(new Date());
        }
            
        startA = System.currentTimeMillis();
        for(tmp = 0; tmp<10000; tmp++)
            al.add(0, new Date());
        stopA = System.currentTimeMillis();
        
        startB = System.currentTimeMillis();
        for(tmp = 0; tmp<10000; tmp++)
            ll.add(0, new Date());
        stopB = System.currentTimeMillis();
        
        
        System.out.println("1)"+(stopA-startA)+" vs "+(stopB-startB));
        
        
        //runda 2
        al = new ArrayList();
        ll = new LinkedList();
        
        for(tmp = 0; tmp<100000; tmp++) {
            al.add(new Date());
            ll.add(new Date());
        }
            
        startA = System.currentTimeMillis();
        for(tmp = 0; tmp<1000; tmp++)
            al.add(80000, new Date());
        stopA = System.currentTimeMillis();
        
        startB = System.currentTimeMillis();
        for(tmp = 0; tmp<1000; tmp++)
            ll.add(80000, new Date());
        stopB = System.currentTimeMillis();
        
        
        System.out.println("2)"+(stopA-startA)+" vs "+(stopB-startB));
        
        
        //runda 3 (tutal LinkedList już nie będzie taki mądry :)
        al = new ArrayList();
        ll = new LinkedList();
            
        startA = System.currentTimeMillis();
        for(tmp = 0; tmp<100000; tmp++)
            al.add(new Date());
        stopA = System.currentTimeMillis();
        
        startB = System.currentTimeMillis();
        for(tmp = 0; tmp<100000; tmp++)
            ll.add(new Date());
        stopB = System.currentTimeMillis();
        
        
        System.out.println("3)"+(stopA-startA)+" vs "+(stopB-startB));
        
        
        //runda 4
        al = new ArrayList();
        ll = new LinkedList();
        
        for(tmp = 0; tmp<30000; tmp++) {
            al.add(new Date());
            ll.add(new Date());
        }
            
        startA = System.currentTimeMillis();
        for(tmp = 0; tmp<30000; tmp++)
            al.remove(0);
        stopA = System.currentTimeMillis();
        
        startB = System.currentTimeMillis();
        for(tmp = 0; tmp<30000; tmp++)
            ll.remove(0);
        stopB = System.currentTimeMillis();
        
        
        System.out.println("4)"+(stopA-startA)+" vs "+(stopB-startB));  
        
        //runda 5 
        al = new ArrayList();
        ll = new LinkedList();
        
        for(tmp = 0; tmp<30000; tmp++) {
            al.add(new Date());
            ll.add(new Date());
        }
            
        startA = System.currentTimeMillis();
        for(tmp = 0; tmp<10000; tmp++)
            al.remove(15000);
        stopA = System.currentTimeMillis();
        
        startB = System.currentTimeMillis();
        for(tmp = 0; tmp<10000; tmp++)
            ll.remove(15000);
        stopB = System.currentTimeMillis();
        
        
        System.out.println("5)"+(stopA-startA)+" vs "+(stopB-startB));   
        
        
        //runda 6 .. a to dziwne :O
        al = new ArrayList();
        ll = new LinkedList();
        
        for(tmp = 0; tmp<120000; tmp++) {
            al.add(new Date());
            ll.add(new Date());
        }
        
        int konec = 120000-1;
            
        startA = System.currentTimeMillis();
        for(tmp = 0; tmp<120000; tmp++)
            al.remove(konec--);
        stopA = System.currentTimeMillis();
        
        konec = 120000-1;
        startB = System.currentTimeMillis();
        for(tmp = 0; tmp<120000; tmp++)
            ll.remove(konec--);
        stopB = System.currentTimeMillis();
        
        
        System.out.println("6)"+(stopA-startA)+" vs "+(stopB-startB));   
        
        
        //runda 7
        al = new ArrayList();
        ll = new LinkedList();
        
        for(tmp = 0; tmp<120000; tmp++) {
            al.add(new Date());
            ll.add(new Date());
        }
        Date d;
            
        startA = System.currentTimeMillis();
        for(tmp = 0; tmp<120000; tmp++)
            d = al.get(0);
        stopA = System.currentTimeMillis();
        
        startB = System.currentTimeMillis();
        for(tmp = 0; tmp<120000; tmp++)
            d = ll.get(0);
        stopB = System.currentTimeMillis();
        
        
        System.out.println("7)"+(stopA-startA)+" vs "+(stopB-startB));  
        
        //runda 8
        al = new ArrayList();
        ll = new LinkedList();
        
        for(tmp = 0; tmp<120000; tmp++) {
            al.add(new Date());
            ll.add(new Date());
        }
            
        startA = System.currentTimeMillis();
        for(tmp = 0; tmp<1200; tmp++)
            d = al.get(60000);
        stopA = System.currentTimeMillis();
        
        startB = System.currentTimeMillis();
        for(tmp = 0; tmp<1200; tmp++)
            d = ll.get(60000);
        stopB = System.currentTimeMillis();
        
        
        System.out.println("8)"+(stopA-startA)+" vs "+(stopB-startB));  
        
        
        //runda 9
        al = new ArrayList();
        ll = new LinkedList();
        
        for(tmp = 0; tmp<120000; tmp++) {
            al.add(new Date());
            ll.add(new Date());
        }
            
        startA = System.currentTimeMillis();
        for(tmp = 0; tmp<120000; tmp++)
            d = al.get(120000-1);
        stopA = System.currentTimeMillis();
        
        startB = System.currentTimeMillis();
        for(tmp = 0; tmp<120000; tmp++)
            d = ll.get(120000-1);
        stopB = System.currentTimeMillis();
        
        
        System.out.println("9)"+(stopA-startA)+" vs "+(stopB-startB));  
    }

}
