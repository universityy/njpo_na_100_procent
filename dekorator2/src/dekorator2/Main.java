package dekorator2;

import java.awt.Point;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        Pieszy pe = new Pieszy(new Point(2,2));
        Rowerzysta er = new Rowerzysta(new Pieszy(new Point(5,5)));
        KierowcaKoparki kaka = new KierowcaKoparki(new Pieszy(new Point(9,9)));
        Scanner s = new Scanner(System.in);
        
        Plansza peDwa = new Plansza() {{
            addUżytkownik(pe);
            addUżytkownik(er);
            addUżytkownik(kaka);
        }};
        
        boolean dalej = true;
        while(dalej) {
            dalej = peDwa.nextTick();
            peDwa.printThePlansza();
            s.nextLine();
        }

    }

}
