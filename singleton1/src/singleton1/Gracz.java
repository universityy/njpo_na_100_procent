package singleton1;

public abstract class Gracz {
    
    public Gracz() {
        
    }
    
    double wygrana(double stawka) {
        stawka = Kasyno.inst().wygrana(stawka);
        return stawka;
    }
    
    double przegrana(double stawka) {
        Kasyno.inst().przegrana(stawka);
        return stawka;
    }
    
    public abstract double graj(double stawka);
}
