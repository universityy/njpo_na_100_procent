
package p101;

import java.util.Map;
import java.util.Objects;


public class NoMemLeak {
    public final String key;
    
    public NoMemLeak(String key) {
        this.key = key;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.key);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NoMemLeak other = (NoMemLeak) obj;
        if (!Objects.equals(this.key, other.key)) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        try {
            Map map = System.getProperties();
            
            for(;;) {
                map.put(new NoMemLeak("key"), "value");
            }
            
        } catch(Exception e) {
            e.printStackTrace();
        }

    }

}
