package kolekcje2;

import kolekcje2.FaceBook;
import kolekcje2.Person;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

public class FaceBookTest {

    private Person p, p2, p3, p4;
    private FaceBook testInstance;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        p = new Person("Zdzisław", "Wodecki", "0700700700");
        p2 = new Person("Zdzisław", "Wodecki", "0700700700");
        p3 = new Person("Stachu", "Kowalski", "707");
        p4 = new Person("Stachu", "Kukuryku", "707");
        testInstance = new FaceBook("test");
        testInstance.add(p);
        testInstance.add(p2);
        testInstance.add(p3);
        testInstance.add(p4);
    }

    @Test
    public void testConstructor() {
        System.out.println("constructor");

        FaceBook instance = new FaceBook("test");
        assertNotNull(instance.getContacts());

        ArrayList<Person> x = new ArrayList();
        instance = new FaceBook("test", x);
        assertSame(x, instance.getContacts());
    }

    @Test
    public void testContains() {
        System.out.println("contains");

        assertFalse(testInstance.contains(new Person("Rychu", "Peja", "5010")));
        assertTrue(testInstance.contains(p));
        assertTrue(testInstance.contains(p2)); //different object, same value
    }

    @Test
    public void testAdd() {
        //add works properly if there are no repetitions
        assertEquals(3, testInstance.getContacts().size());
    }

    @Test
    public void testFind() {
        System.out.println("find");
        
        ArrayList find;
        
        find = testInstance.find("Stachu", null, null);
        assertEquals(2, find.size());
        
        find = testInstance.find(null, "Wodecki", null);
        assertEquals(1, find.size());
        
        find = testInstance.find(null, "Wodecki", "707");
        assertEquals(0, find.size());
       
        find = testInstance.find(null, null, "707");
        assertEquals(2, find.size());
    }

    @Test
    public void testSort() {        
        System.out.println("sort");
        
        assertEquals("Zdzisław", testInstance.getContacts().get(0).getName());
        testInstance.sort();
        assertEquals("Stachu", testInstance.getContacts().get(0).getName());
    }

    @Test
    public void testClear() {
        System.out.println("clear");
        
        testInstance.clear();
        
        assertEquals(0, testInstance.getContacts().size());
    }
    
    @Test
    public void testRemove() {
        System.out.println("remove");
        
        FaceBook instance = new FaceBook("test2");
        instance.add(p);
        instance.add(p3);
        
        instance.remove(p2);
        
        assertEquals(1, instance.getContacts().size());
    }
    
    @Test
    public void testDebugContacts() {
        System.out.println("debugContacts");
        
        PrintStream oldOut = System.out;
        
        String target = "--> Book test\n-> Zdzisław Wodecki (0700700700)\n"+
                "-> Stachu Kowalski (707)\n"+
                "-> Stachu Kukuryku (707)\n\n";
        
        System.setOut(new PrintStream(outContent));
        testInstance.debugContacts();
        System.setOut(oldOut);
        
        assertEquals(target, outContent.toString());
    }

}
