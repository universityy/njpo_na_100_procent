package fabryka1;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;

public class GalleryPage extends WebPage {

    private static final String[] images = new String[]{"black.png", "cyan.png",
        "green.png"};

    @Override
    public void writeToStream(Writer w) throws IOException {
        w.write(header("Gallery v1.0"));

        for (int t = 0; t < 20; t++) {
            w.write("<img src=\"img/" + images[(int) (Math.random() * images.length)] + "\"><br>");
        }

        w.write(endPage());
    }

}
