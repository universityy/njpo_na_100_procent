package gui2;

public class Main extends javax.swing.JFrame {

    /**
     * Creates new form Main
     */
    public Main() {
        initComponents();
        wynikPole.setEditable(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        dodajButton = new javax.swing.JRadioButton();
        odejmijButton = new javax.swing.JRadioButton();
        pomnozButton = new javax.swing.JRadioButton();
        podzielButton = new javax.swing.JRadioButton();
        wykonajButton = new javax.swing.JButton();
        wynikPole = new javax.swing.JTextField();
        liczba1 = new javax.swing.JFormattedTextField();
        liczba2 = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Kalkulator");

        buttonGroup1.add(dodajButton);
        dodajButton.setSelected(true);
        dodajButton.setText("Dodaj");

        buttonGroup1.add(odejmijButton);
        odejmijButton.setText("Odejmij");

        buttonGroup1.add(pomnozButton);
        pomnozButton.setText("Pomnóż");

        buttonGroup1.add(podzielButton);
        podzielButton.setText("Podziel");

        wykonajButton.setText("Wykonaj");
        wykonajButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wykonajButtonActionPerformed(evt);
            }
        });

        wynikPole.setText("Wynik");

        liczba1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.#######"))));
        liczba1.setText("0");

        liczba2.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.########"))));
        liczba2.setText("0");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(liczba1)
                    .addComponent(wykonajButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(wynikPole)
                    .addComponent(liczba2))
                .addGap(8, 8, 8))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(187, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(podzielButton)
                    .addComponent(pomnozButton)
                    .addComponent(dodajButton)
                    .addComponent(odejmijButton, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(185, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(liczba1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(dodajButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(odejmijButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pomnozButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(podzielButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(liczba2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(wykonajButton)
                .addGap(18, 18, 18)
                .addComponent(wynikPole, javax.swing.GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
                .addGap(24, 24, 24))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void wykonajButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_wykonajButtonActionPerformed
        String tmp = liczba1.getText();
        double a = tmp.isEmpty() ? 0 : Double.parseDouble(tmp);
        tmp = liczba2.getText();
        double b = tmp.isEmpty() ? 0 : Double.parseDouble(tmp);
        
        double result = 0;
        
        if (dodajButton.isSelected())
            result = a+b;
        else if (odejmijButton.isSelected())
            result = a-b;
        else if (pomnozButton.isSelected())
            result = a*b;
        else
            result = a/b;
        
        wynikPole.setText(Double.toString(result));
        
    }//GEN-LAST:event_wykonajButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JRadioButton dodajButton;
    private javax.swing.JFormattedTextField liczba1;
    private javax.swing.JFormattedTextField liczba2;
    private javax.swing.JRadioButton odejmijButton;
    private javax.swing.JRadioButton podzielButton;
    private javax.swing.JRadioButton pomnozButton;
    private javax.swing.JButton wykonajButton;
    private javax.swing.JTextField wynikPole;
    // End of variables declaration//GEN-END:variables
}
