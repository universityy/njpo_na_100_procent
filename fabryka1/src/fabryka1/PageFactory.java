package fabryka1;

public class PageFactory {
    public static WebPage createPage(PageType type) {
        if (PageType.GALLERY.equals(type)) {
            return new GalleryPage();
        } else if (PageType.INFO.equals(type)) {
            return new InfoPage();
        } else if (PageType.CONTACT.equals(type)) {
            return new ContactPage();
        } else {
            return new NewsPage();
        }
    }
}
