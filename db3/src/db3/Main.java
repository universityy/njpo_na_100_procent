package db3;

import java.sql.*;
import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void prepareDB(int cnt) throws SQLException {
        Random r = new Random();

        DBDriver db = DBDriver.getDriver();

        db.update("create table if not exists sorttest (id bigint auto_increment, data bigint, primary key (id))");
        db.update("truncate table sorttest");

        String query = "insert into sorttest values(null, ?),(null, ?),(null, ?),(null, ?),(null, ?),(null, ?),(null, ?),(null, ?),(null, ?),(null, ?),(null, ?),(null, ?),(null, ?),(null, ?),(null, ?),(null, ?),(null, ?),(null, ?),(null, ?),(null, ?)";

        Connection con = DBDriver.getDriver().getConnection();
        con.setAutoCommit(false);

        cnt /= 20;
        try (PreparedStatement p = con.prepareStatement(query)) {
            while (cnt-- > 0) { // --> operator, (C) 2015
                p.setLong(1, r.nextLong());
                p.setLong(2, r.nextLong());
                p.setLong(3, r.nextLong());
                p.setLong(4, r.nextLong());
                p.setLong(5, r.nextLong());
                p.setLong(6, r.nextLong());
                p.setLong(7, r.nextLong());
                p.setLong(8, r.nextLong());
                p.setLong(9, r.nextLong());
                p.setLong(10, r.nextLong());
                p.setLong(11, r.nextLong());
                p.setLong(12, r.nextLong());
                p.setLong(13, r.nextLong());
                p.setLong(14, r.nextLong());
                p.setLong(15, r.nextLong());
                p.setLong(16, r.nextLong());
                p.setLong(17, r.nextLong());
                p.setLong(18, r.nextLong());
                p.setLong(19, r.nextLong());
                p.setLong(20, r.nextLong());

                p.addBatch();

                if (cnt % 2000 == 0) {
                    p.executeBatch();
                    con.commit();
                }
            }
        }
       
        
    }

    public static void main(String[] args) throws Exception {
        /*
         * so:
         * 1. prepare DB which will be called if needed
         * 2. time for selecting the results via array + select count
         * 3. and the same, with sorting the array.
         */
        int cnt = 2000000;
        long[] data = new long[cnt];
        
        try {
            DBDriver driver = DBDriver.getDriver();
            System.out.println("Preparing battle arena....");
            
            prepareDB(cnt+200);
            
            System.out.println("Java vs MySQL... let's start!");
        
            //get the data and sort them here
            int tmp = 0;
            long a, b, c, d;

            a = System.currentTimeMillis();
            ResultSet rs = driver.query("select data from sorttest limit 1, "+cnt);
            while(rs.next()) {
                data[tmp++] = rs.getLong(1);
            }
            Arrays.sort(data);
            driver.closeResult(rs);
            b = System.currentTimeMillis();

            c = System.currentTimeMillis();
            tmp = 0;
            rs = driver.query("select data from sorttest order by data asc limit 1, "+cnt);
            while(rs.next()) {
                data[tmp++] = rs.getLong(1);
            }
            driver.closeResult(rs);
            d = System.currentTimeMillis();


            System.out.println("Result: "+(b - a) + " vs " + (d - c));
            
            
        } catch (Exception e) {
            System.out.println("Show canceled :("+e.getMessage());
            e.printStackTrace();
        }
        
        

    }

}
