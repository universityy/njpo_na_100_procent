
package testy1;

import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        
        System.out.println("Podaj a:");
        double a = s.nextDouble();
        System.out.println("Podaj b:");
        double b = s.nextDouble();
        System.out.println("Podaj c:");
        double c = s.nextDouble();
        
        double[] solution = new QESolver(a,b,c).getRoots();
        System.out.flush();
        
        if (solution == null) {
            System.out.println("brak rozwiązań");
        } else {
            if (solution.length == 2)
                System.out.println("2 rozwiązania: "+solution[0]+", "+solution[1]);
            else
                System.out.println("rozwiązanie: "+solution[0]);
        }
    }

}
