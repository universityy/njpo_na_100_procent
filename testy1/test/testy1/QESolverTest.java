package testy1;

import testy1.QESolver;
import org.junit.Test;
import static org.junit.Assert.*;

public class QESolverTest {
    
    public QESolverTest() {
    }
    
    @Test
    public void testQuadraticConstructor() {
        System.out.println("constructor");
        QESolver instance = new QESolver(1, 2, 3);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testLinearConstructor() {
        System.out.println("linear constructor");
        QESolver instance = new QESolver(0, 2, 3);
    }
    
    @Test
    public void testGetDelta() {
        System.out.println("getDelta");
        assertEquals(4.0, new QESolver(1, 4, 3).getDelta(), 0.0);
        assertEquals(0.0, new QESolver(1, 4, 4).getDelta(), 0.0);
        assertEquals(-4.0, new QESolver(1, 4, 5).getDelta(), 0.0);
    }

    @Test
    public void testNoOfRoots() {
        System.out.println("noOfRoots");
        assertEquals(2, new QESolver(1, 4, 3).noOfRoots());
        assertEquals(1, new QESolver(1, 4, 4).noOfRoots());
        assertEquals(0, new QESolver(1, 4, 5).noOfRoots());
    }

    @Test
    public void testGetRoots() {
        System.out.println("getRoots");
        assertNull(new QESolver(2, 2, 1).getRoots());
        assertArrayEquals(new double[] {-1.0, 2.0}, new QESolver(1, -1, -2).getRoots(), 0.01);
        assertArrayEquals(new double[] {-2.0}, new QESolver(1, 4, 4).getRoots(), 0.01);
    }
    
}
