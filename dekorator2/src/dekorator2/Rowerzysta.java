package dekorator2;

import java.awt.Point;

public class Rowerzysta extends Dekorator {

    public Rowerzysta(UżytkownikDrogi user) {
        super(user);
    }

    @Override
    public char getToken() {
        return 'R';
    }

    @Override
    public Point getNextLocation() {
        int deltaY = 0;
        if (Math.random()<0.2) {
            deltaY = (Math.random()<0.5) ? -1 : 1;
        }
        
        Point ret = new Point(location);
        
        ret.x += 2;
        ret.y += deltaY;
        
        return ret;
    }

    
}
