package testy1;

public class QESolver {
    private double a, b, c;
    
    public QESolver(double a, double b, double c) {
        if (a == 0)
            throw new IllegalArgumentException("This is not a quadratic equation.");
        
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public double getDelta() {
        return b*b - 4*a*c;
    }
    
    public int noOfRoots() {
        double delta = getDelta();
        return delta < 0 ? 0 : delta == 0 ? 1 : 2;
    }
    
    public double[] getRoots() {
        int noRoots = noOfRoots();
        if (noRoots == 0) return null;
        
        double ddelta = Math.sqrt(getDelta());
        
        if (noRoots == 1)
            return new double[] {(-b)/(2*a)};
        else
            return new double[] {(-b-ddelta)/(2*a), (-b+ddelta)/(2*a)};
    }
    
}
