package fabryka1;

import java.io.IOException;
import java.io.Writer;

public class InfoPage extends WebPage {
    
    //still better than most newspapers...
    protected String sentenceOfNonsense() {
        int numWords = (int)(Math.random()*20) + 6;
        boolean first = true;
        StringBuilder sb = new StringBuilder();
        
        while (numWords --> 0) {
            if (!first)
                sb.append(" ");
            
            sb.append(genWord(first));
            
            first = false;
        }
        sb.append(". ");
        
        return sb.toString();
    }
    
    protected String genWord(boolean capitalize) {
        int numChars = (int)(Math.random()*6) + 4;
        StringBuilder sb = new StringBuilder();
        
        while (numChars --> 0) {
            sb.append((char)((int)(Math.random()*26)+(capitalize?65:97)));
            capitalize = false;
        }
        return sb.toString();
    }
    
    protected void writeNews(Writer w, int noSentences) throws IOException {
        while(noSentences --> 0) {
            w.write(sentenceOfNonsense());
        }
    }
    
    @Override
    public void writeToStream(Writer w) throws IOException {
        w.write(header("Info!"));
        
        writeNews(w, (int)(Math.random()*500) + 500);
        
        w.write(endPage());
    }
    
    
    
}
