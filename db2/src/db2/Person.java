package db2;

import java.io.Serializable;
import java.util.Objects;

public class Person implements Comparable, Serializable {

    private String name;
    private String surname;
    private String phone;

    public Person(String name, String surname, String phone) {
        setName(name);
        this.surname = surname;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException();
        }

        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public String getDisplay() {
        return this.name + (this.surname != null ? " " + this.surname : "");
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public int compareTo(Object t) {
        Person p = (Person) t;

        String c1 = this.surname != null ? this.surname : this.name;
        String c2 = p.surname != null ? p.surname : p.name;

        int compare = c1.compareTo(c2);
        if (compare == 0) {
            compare = this.name.compareTo(p.name);
        }

        return compare;
    }

    @Override
    public String toString() {
        return name + (surname != null ? " " + surname : "") + (phone != null ? " (" + phone + ")": "");
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + Objects.hashCode(this.surname);
        hash = 97 * hash + Objects.hashCode(this.phone);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.surname, other.surname)) {
            return false;
        }
        if (!Objects.equals(this.phone, other.phone)) {
            return false;
        }
        return true;
    }

}
