package singleton1;

public class GraczWBlackjacka extends Gracz {
    
    @Override
    public double graj(double stawka) {
        double wynik;
        if (Math.random()>0.5) {
            wynik = wygrana(2*stawka);
        } else {
            wynik = -przegrana(stawka);
        }
        return wynik;
    }
    
}
