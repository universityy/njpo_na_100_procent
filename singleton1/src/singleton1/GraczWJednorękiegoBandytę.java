package singleton1;

public class GraczWJednorękiegoBandytę extends Gracz {
    
    @Override
    public double graj(double stawka) {
        double wynik;
        if (Math.random()>0.9) {
            wynik = wygrana(3*stawka);
        } else {
            wynik = -przegrana(stawka);
        }
        return wynik;
    }
    
}
