package db2;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.sql.*;

public class FaceBookDB extends FaceBook {

    public FaceBookDB(String id) {
        super(id);
    }

    public FaceBookDB(String id, ArrayList contacts) {
        super(id, contacts);
    }

    public void ensureTables() throws SQLException {
        DBDriver db = DBDriver.getDriver();

        db.update("create table if not exists fbdb (name varchar(45) not null, recordid int not null, person blob not null, primary key (name, recordid))");
    }

    public synchronized void save() throws SQLException, IOException {
        ensureTables();

        Connection con = DBDriver.getDriver().getConnection();
        String query = "insert into fbdb(name, recordid, person) values(?, ?, ?) on duplicate key update person=values(person)";
        
        try (PreparedStatement statement = con.prepareStatement(query)) {
            int count = 0;

            for (Person p : contacts) {
                statement.setString(1, id);
                statement.setInt(2, count + 1);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                oos.writeObject(p);

                statement.setBytes(3, baos.toByteArray());

                statement.addBatch();
                count++;
            }

            statement.executeBatch();

            try (PreparedStatement del = con.prepareStatement("delete from fbdb where name=? and recordid>?")) {
                del.setString(1, id);
                del.setInt(2, count);

                del.execute();
            }
        }

    }

    public synchronized void load() throws SQLException, IOException, ClassNotFoundException {
        ensureTables();

        Connection con = DBDriver.getDriver().getConnection();
        String query = "select person from fbdb where name=? order by recordid asc";

        try (PreparedStatement statement = con.prepareStatement(query)) {
            statement.setString(1, id);
            try (ResultSet rs = statement.executeQuery()) {
                clear();

                while (rs.next()) {
                    ByteArrayInputStream bais = new ByteArrayInputStream(rs.getBytes(1));
                    ObjectInputStream ois = new ObjectInputStream(bais);
                    Person p = (Person) ois.readObject();
                    add(p);
                }
            }
        }
    }
    
    public synchronized void eraseDB() throws SQLException, IOException, ClassNotFoundException {
        ensureTables();
        
        Connection con = DBDriver.getDriver().getConnection();
        String query = "delete from fbdb where name=?";
        
        try (PreparedStatement statement = con.prepareStatement(query)) {
         statement.setString(1, id);
         statement.execute();
        }
    }

}
