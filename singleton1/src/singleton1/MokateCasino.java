package singleton1;

public class MokateCasino {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Kasyno mokate = Kasyno.inst();
        GraczWBlackjacka zdzisiek = new GraczWBlackjacka();
        GraczWJednorękiegoBandytę mietek = new GraczWJednorękiegoBandytę();
        double wynik;
        
        mokate.printStanKasy();
        
        System.out.println("Zdzichu gra!");
        wynik = zdzisiek.graj(500);
        System.out.println("Zdzichu "+(wynik>0 ? "wygrał" : "przegrał")+" "+Math.abs(wynik)+" dolców!");
        
        mokate.printStanKasy();
                
        System.out.println("Mietek gra!");
        wynik = zdzisiek.graj(500);
        System.out.println("Mietek "+(wynik>0 ? "wygrał" : "przegrał")+" "+Math.abs(wynik)+" dolców!");
        
        mokate.printStanKasy();
    }
        
}
