package watki1;

import java.io.*;

class ZipThread implements Runnable {

    @Override
    public void run() {
        File f;
        FileOutputStream fo = null;
        try {
            f = File.createTempFile("tmp", ".zb3bomb", null);

            fo = new FileOutputStream(f);

            int bytesLeft = 1048576; //mebibyte :]

            while (bytesLeft-- > 0) {
                fo.write((int) Math.floor(Math.random() * 256));
            }

        } catch (IOException e) {
        } finally {
            try {
                fo.close();
            } catch (IOException e) {

            }
        }

    }

}

public class Main {

    public static void main(String[] args) {
        //this has no effect on my PC even tho it's old as hell

        for (int t = 0; t < 10; t++) {
            new Thread(new ZipThread()).start();
        }
    }

}
