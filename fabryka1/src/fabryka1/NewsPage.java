package fabryka1;

import java.io.IOException;
import java.io.Writer;

public class NewsPage extends InfoPage {

    @Override
    public void writeToStream(Writer w) throws IOException {
        w.write(header("World News Biased news"));
        
        int numNews = 30;
        
        while (numNews --> 0) {
            w.write("<b>"+sentenceOfNonsense()+"</b><br><br>");
            writeNews(w, (int)(Math.random()*30) + 30);
            w.write("<hr><br>");
        }
        
        w.write(endPage());
    }
    
}
