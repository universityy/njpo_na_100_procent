package dekorator2;

import java.awt.Point;

public class KierowcaKoparki extends Dekorator {

    public KierowcaKoparki(UżytkownikDrogi user) {
        super(user);
    }
    
    @Override
    public char getToken() {
        return 'K';
    }

    @Override
    public Point getNextLocation() {
        int deltaY = 0;
        if (Math.random()<0.3) {
            deltaY = (Math.random()<0.5) ? -2 : 2;
        }
        
        Point ret = new Point(location);
        
        ret.x += 3;
        ret.y += deltaY;
        
        return ret;
    }
}
