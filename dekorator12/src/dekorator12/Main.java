package dekorator12;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.StringTokenizer;


public class Main {

    public static void main(String[] args) {
        try (BufferedReader br = new BufferedReader(new FileReader("/etc/passwd"))) {
            String tmp;
            StringTokenizer tokenizer;
            int ret = 0;
            
            while((tmp = br.readLine()) != null) {
                tokenizer = new StringTokenizer(tmp, " \t\n");
                ret += tokenizer.countTokens();
            }
            
            System.out.println("Liczba słów: "+ret);
            
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }

    }

}
